﻿using System;
using Etapa1.com.platzi.bl;
using Etapa1.com.platzi.tl;
using static Etapa1.util.Impresor;

namespace Etapa1.com.platzi.ui
{
    class Program
    {
        private static EscuelaEngine escuelaEngine = new EscuelaEngine();
        private static AlumnoEngine alumnoEngine = new AlumnoEngine();
        private static CursoEngine cursoEngine = new CursoEngine();
        private static AsignaturaEngine asignaturaEngine = new AsignaturaEngine();
        private static EvaluacionEngine evaluacionesEngine = new EvaluacionEngine();
        

        private static void Main(string[] args)
        {
            Beep(2);
            ImprimirTitulo("BIENVENIDO");
            ListarMenu();
        }


        private static void ListarMenu()
        {
            byte opcion;

            do
            {

                ImprimirLinea(Environment.NewLine);
                ImprimirLinea(50);

                ImprimirLinea("1. Registrar nueva escuela");
                ImprimirLinea("2. Ver escuela");
                ImprimirLinea("3. Registrar asignatura");
                ImprimirLinea("4. Listar asignaturas");
                ImprimirLinea("5. Registrar curso");
                ImprimirLinea("6. Listar cursos");
                ImprimirLinea("7. Registrar alumno");
                ImprimirLinea("8. Listar alumnos");
                ImprimirLinea("9. Registrar evaluación");
                ImprimirLinea("10. Listar evaluaciones");
                ImprimirLinea("0. Salir");
                
                ImprimirLineaMargen($"Digite una opción");
                
                ImprimirLinea(50);
                
                opcion = Byte.Parse(LeerLinea());
                
                EjecutarOpcion(opcion);
                
            } while (opcion != 0);
        }

        private static void EjecutarOpcion(byte popcion)
        {
            switch (popcion)
            {
                case 1:
                    RegistrarEscuela();
                    break;
                case 2:
                    ListarEscuela();
                    break;
                case 3:
                    RegistrarAsignatura();
                    break;
                case 4:
                    ListarAsigntaturas();
                    break;
                case 5:
                    RegistrarCurso();
                    break;
                case 6:
                    ListarCursos();
                    break;
                case 7:
                    RegistrarAlumno();
                    break;
                case 8:
                    ListarAlumnos();
                    break;
                case 9:
                    RegistrarEvaluacion();
                    break;
                case 10:
                    ListarEvaluaciones();
                    break;
                case 0:
                    ImprimirLineaMargen($"Ciao.");
                    break;
                
                default:
                    ImprimirLineaMargen($"Opción inválida.");
                    break;
            }
        }

        private static void RegistrarEscuela()
        {
            ImprimirLineaMargen("Escriba el nombre");
            var nombre = LeerLinea();
            
            ImprimirLineaMargen("Escriba el año de creación");
            var anioCreacion = int.Parse(LeerLinea());
            
            ImprimirLineaMargen("Escriba el país");
            var pais = LeerLinea();
            
            ImprimirLineaMargen("Escriba la ciudad");
            var ciudad = LeerLinea();

            TipoEscuela tipo = PedirTipoEscuela();
            
            escuelaEngine.RegistrarEscuela(nombre, anioCreacion, pais, ciudad, tipo);
        }

        private static TipoEscuela PedirTipoEscuela()
        {
            while (true)
            {
                ImprimirLineaMargen("Seleccione el tipo de escuela");

                ImprimirLineaMargen("1. Primaria");
                ImprimirLinea("2. Secundaria");
                ImprimirLinea("3. Preescolar");

                var tipoSeleccionado = int.Parse(LeerLinea());

                switch (tipoSeleccionado)
                {
                    case 1:
                        return TipoEscuela.PRIMARIA;
                    case 2:
                        return TipoEscuela.SECUNDARIA;
                    case 3:
                        return TipoEscuela.PREESCOLAR;
                    default:
                        continue;
                }
            }
        }
        
        private static TipoJornada PedirTipoJornada()
        {
            while (true)
            {
                ImprimirLineaMargen("Seleccione el tipo de jornada");

                ImprimirLineaMargen("1. Mañana");
                ImprimirLinea("2. Tarde");
                ImprimirLinea("3. Noche");

                var tipoSeleccionado = int.Parse(LeerLinea());

                switch (tipoSeleccionado)
                {
                    case 1:
                        return TipoJornada.MAÑANA;
                    case 2:
                        return TipoJornada.TARDE;
                    case 3:
                        return TipoJornada.NOCHE;
                    default:
                        continue;
                }
            }
        }

        private static void RegistrarCurso()
        {
            if (ListarAsigntaturas() > 0)
            {
                ImprimirLineaMargen("Escriba el id de la asignatura del curso");
                var idAsignatura = LeerLinea();
                
                Asignatura tmpAsignatura;
                if ((tmpAsignatura = asignaturaEngine.GetAsignatura(idAsignatura)) != null)
                {
                    ImprimirLineaMargen("Escriba el nombre del curso");
                    var nombre = LeerLinea();

                    TipoJornada tipoJornada = PedirTipoJornada();
                    
                    cursoEngine.RegistrarCurso(nombre, tipoJornada, tmpAsignatura);
                }
                else
                {
                    ImprimirLineaMargen("No se encontró la asignatura");
                }
            }
            else
            {
                ImprimirLineaMargen("Se necesita que exista al menos una asignatura para poder registrar un curso");
            }
        }

        public static void RegistrarAsignatura()
        {
            ImprimirLineaMargen("Escriba el nombre");
            var nombre = LeerLinea();
            asignaturaEngine.RegistrarAsignatura(nombre);
        }

        public static void RegistrarAlumno()
        {
            ImprimirLineaMargen("Escriba el nombre completo");
            var nombre = LeerLinea();
            alumnoEngine.RegistrarAlumno(nombre);
        }

        public static void RegistrarEvaluacion()
        {
            if (ListarAlumnos() > 0 && ListarCursos() >0)
            {
                
                ImprimirLineaMargen("Escriba el id del curso");
                var idCurso = LeerLinea();

                Curso curso;
                if ((curso = cursoEngine.GetCurso(idCurso)) != null)
                {
                    ImprimirLineaMargen("Escriba el id del alumno");
                    var idAlumno = LeerLinea();

                    Alumno al;
                    if ((al = alumnoEngine.GetAlumno(idAlumno)) != null)
                    {
                        ImprimirLineaMargen("Escriba la nota del alumno en el curso");
                        var nota = int.Parse(LeerLinea());
                        if (nota > 0)
                        {
                            evaluacionesEngine.RegistrarEvaluacion(nota, curso, al);
                        }
                        else
                        {
                            ImprimirLineaMargen("Error. Nota negativa rechazada/");
                        }
                    }
                    else
                    {
                        ImprimirLineaMargen("Alumno no encontrado");
                    }
                }
                else
                {
                    ImprimirLineaMargen("Curso no encontrado");
                }
            }
            else
            {
                ImprimirLineaMargen("Se necesita al menos un alumno y un curso para registrar una evaluación");
            }
        }
        
        private static void ListarEscuela()
        {
            ImprimirLineaMargen(escuelaEngine.GetEscuela());
        }
        
        
        private static int ListarAlumnos()
        {
            var data = alumnoEngine.GetAlumnos();

            ImprimirLineaMargen("Alumnos: ");
            
            foreach (var dato in data)
            {
                ImprimirLineaMargen(dato);
                if (dato == "No se encontraron alumnos")
                {
                    return 0;
                }
            }
            return data.Length;
        }
        
        private static int ListarAsigntaturas()
        {
            var data = asignaturaEngine.GetAsignaturas();

            ImprimirLineaMargen("Asignaturas: ");

            foreach (var dato in data)
            {
                ImprimirLineaMargen(dato);
                if (dato == "No se encontraron asignaturas")
                {
                    return 0;
                }
            }
            return data.Length;
        }
        
        private static int ListarCursos()
        {
            var data = cursoEngine.GetCursos();

            ImprimirLineaMargen("Cursos: ");

            foreach (var dato in data)
            {
                ImprimirLineaMargen(dato);
                if (dato == "No se encontraron cursos")
                {
                    return 0;
                }
            }
            return data.Length;
        } 
        
        private static int ListarEvaluaciones()
        {
            var data = evaluacionesEngine.GetEvaluaciones();

            ImprimirLineaMargen("Evaluaciones: ");

            foreach (var dato in data)
            {
                ImprimirLineaMargen(dato);
                if (dato == "No se encontraron evaluaciones")
                {
                    return 0;
                }
            }
            return data.Length;
        } 
    }
}