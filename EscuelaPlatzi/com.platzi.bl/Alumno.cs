using System;

namespace Etapa1.com.platzi.bl
{
    public class Alumno : EntidadEscolar
    {
        public Alumno() { }
        
        public Alumno(string nombre)
        {
            Nombre = nombre;
        }

        public override string ToString()
        {
            return $"Id: {UniqueId}, Nombre: {Nombre}";
        }
    }
}