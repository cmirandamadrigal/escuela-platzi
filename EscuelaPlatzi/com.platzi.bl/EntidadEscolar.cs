using System;

namespace Etapa1.com.platzi.bl
{
    public abstract class EntidadEscolar
    {
        public string Nombre { get; set; }
        
        public string UniqueId { get; private set; }
        
        public EntidadEscolar()
        {
            UniqueId = Guid.NewGuid().ToString();
        }
    }
}