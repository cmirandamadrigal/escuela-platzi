using System;

namespace Etapa1.com.platzi.bl
{
    public class Curso : EntidadEscolar
    {
        
        private TipoJornada _jornada;

//        public string UniqueId {  get => _uniqueId; private set => _uniqueId = value; }

//        public string Nombre { get => _nombre; set => _nombre = value; }
        
        public Asignatura Asignatura { get; set; }

        public TipoJornada Jornada { get => _jornada; set => _jornada = value; }

        public Curso() { }

        public override string ToString()
        {
            return $"Id = {UniqueId}, nombre = {Nombre}, jornada = {Jornada}, asignatura = {Asignatura}";
        }
    }
}