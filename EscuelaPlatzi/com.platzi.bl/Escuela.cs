using System;
using System.Collections.Generic;
using System.Text;
using static System.Console;

namespace Etapa1.com.platzi.bl
{
    public class Escuela : EntidadEscolar
    {
        
        private int _anioCreacion;

        private string _pais;

        private string _ciudad;

        private TipoEscuela _tipoEscuela;

        private List<Curso> _cursos;

        private List<Asignatura> _asignaturas;

        private List<Alumno> _alumnos;
        
        private List<Evaluacion> _evaluaciones;
        
        
        public Escuela()
        {
            Nombre = "No c";
            AnioCreacion = 1900;
            Pais = "Mongolia";
            Ciudad = "Tinoca";
            TipoEscuela = TipoEscuela.PRIMARIA;
            Cursos = new List<Curso>();
            Alumnos = new List<Alumno>();
            Asignaturas = new List<Asignatura>();
            Evaluaciones = new List<Evaluacion>();
        }
        
        public Escuela(string nombre, int anioCreacion, string pais, string ciudad, TipoEscuela tipoEscuela)
        {
            _anioCreacion = anioCreacion;
            _pais = pais;
            _ciudad = ciudad;
            _tipoEscuela = tipoEscuela;
            Cursos = new List<Curso>();
            Alumnos = new List<Alumno>();
            Asignaturas = new List<Asignatura>();
            Evaluaciones = new List<Evaluacion>();
        }
        
        public Escuela(string nombre, int anioCreacion, TipoEscuela tipoEscuela, 
            string pais = "no c", string ciudad = "no c")
        {
           (Nombre, AnioCreacion, TipoEscuela) = (nombre, anioCreacion, tipoEscuela);
            Pais = pais;
            Ciudad = ciudad;
            Cursos = new List<Curso>();
            Alumnos = new List<Alumno>();
            Asignaturas = new List<Asignatura>();
            Evaluaciones = new List<Evaluacion>();
        }
        
        public int AnioCreacion { get => _anioCreacion; set => _anioCreacion = value; }
        
        public string Pais { get => _pais; set => _pais = value; }
        
        public string Ciudad { get => _ciudad; set => _ciudad = value; }

        public TipoEscuela TipoEscuela { get => _tipoEscuela; set => _tipoEscuela = value; }

        public List<Curso> Cursos { get => _cursos; set => _cursos = value; }
        
        public List<Asignatura> Asignaturas { get => _asignaturas; set => _asignaturas = value; }

        public List<Alumno> Alumnos { get => _alumnos; set => _alumnos = value; }

        public List<Evaluacion> Evaluaciones { get => _evaluaciones; set => _evaluaciones = value; }

        public override string ToString()
        {
            StringBuilder tmpCursos = new StringBuilder($"{Environment.NewLine}Cursos: {Environment.NewLine}");
            Cursos.ForEach(c => tmpCursos.Append($"|{c}|"));
            StringBuilder tmpAlumnos = new StringBuilder($"{Environment.NewLine}Alumnos: {Environment.NewLine}");
            Alumnos.ForEach(c => tmpAlumnos.Append($"{c}"));
            StringBuilder tmpAsignaturas = new StringBuilder($"{Environment.NewLine}Asignaturas: {Environment.NewLine}");
            Asignaturas.ForEach(c => tmpAsignaturas.Append($"{c}"));
            return $"Id: ${UniqueId}, Nombre: \"{Nombre}\", Tipo:  {_tipoEscuela}, Año fundación: {_anioCreacion}, " +
                   $"País:  {_pais}, Ciudad:  {_ciudad} {tmpCursos} {tmpAlumnos} {tmpAsignaturas}";
        }
        
        public void ImprimirCursosEscuela()
        {
            WriteLine($"{Environment.NewLine}Cursos de la escuela {Nombre}: {Environment.NewLine}");

            if (Cursos != null || Cursos.Count > 0)
            {
                foreach (var curso in Cursos)
                {
                    WriteLine(curso.ToString());   
                }
            }
            else
            {
                WriteLine($"La escuela ${Nombre} no tiene cursos.");
            }
            
           
        }
    }
}