namespace Etapa1.com.platzi.bl
{
    public enum TipoJornada
    {
        MAÑANA,
        TARDE,
        NOCHE
    }
}