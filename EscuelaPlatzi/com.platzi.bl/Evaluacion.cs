using System;

namespace Etapa1.com.platzi.bl
{
    public class Evaluacion : EntidadEscolar
    {
        public Evaluacion()
        {
        }
        
        public Evaluacion(int nota, Curso curso, Alumno alumno)
        {
            Nota = nota;
            Curso = curso;
            Alumno = alumno;
        }
        
        
        public int Nota { get; set; }
        
        public Curso Curso { get; set; }
        
        public Alumno Alumno { get; set; }

        public override string ToString()
        {
            return $"Id: {UniqueId}, Asignatura: {Curso}, Alumno: {Alumno.ToString()}, Nota: {Nota}";
        }
    }
}