using System;

namespace Etapa1.com.platzi.bl
{
    public class Asignatura : EntidadEscolar
    {
        
        public Asignatura() { }
        
        public Asignatura(string nombre) { Nombre = nombre; }

        public override string ToString()
        {
            return $"Id: {UniqueId}, Nombre: {Nombre}";
        }
    }
}