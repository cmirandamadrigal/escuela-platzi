using System.Collections.Generic;
using Etapa1.com.platzi.bl;

namespace Etapa1.com.platzi.dl
{
    public class CL
    {
        private Escuela _escuela;

        public CL()
        {
            Inicializar();
        }

        private void Inicializar()
        {
            Escuela = new Escuela();
        }

        public void RegistrarEscuela(Escuela escuela)
        {
            Escuela = escuela;
        }
        
        public void RegistrarCurso(Curso curso)
        {
            Escuela.Cursos.Add(curso);
        }
        
        public void RegistrarAlumno(Alumno alumno)
        {
            Escuela.Alumnos.Add(alumno);
        }
        
        public void RegistrarAsignatura(Asignatura asignatura)
        {
            Escuela.Asignaturas.Add(asignatura);
        }
        
        public void RegistrarEvaluacion(Evaluacion evaluacion)
        {
            Escuela.Evaluaciones.Add(evaluacion);
        }
        
        public Asignatura GetAsignatura(string idAsignatura)
        {
            return Escuela.Asignaturas.Find(a => a.UniqueId == idAsignatura);
        }
        
        public Curso GetCurso(string idCurso)
        {
            return Escuela.Cursos.Find(a => a.UniqueId == idCurso);
        }

        public Alumno GetAlumno(string idAlumno)
        {
            return Escuela.Alumnos.Find(a => a.UniqueId == idAlumno);
        }
        
        public Escuela Escuela { get => _escuela; set => _escuela = value; }

    }
}