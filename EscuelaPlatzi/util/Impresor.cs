using System;
using static System.Console;

namespace Etapa1.util
{
    public static class Impresor
    {
        
        public static void ImprimirTitulo(string titulo)
        {
            var tamanio = titulo.Length + 4;
            ImprimirLinea(tamanio);
            ImprimirLinea($"| {titulo} |");
            ImprimirLinea(tamanio);
        }
        
        public static void ImprimirLinea(int tam)
        {
            WriteLine("".PadLeft(tam, '='));
        }
        
        public static void ImprimirLinea(string texto)
        {
            WriteLine(texto);
        }

        public static void ImprimirLineaMargen(string texto)
        {
            WriteLine($"{Environment.NewLine}{texto}");
        }

        public static void Beep(int cantidad)
        {
            while (cantidad-- > 0)
            {
                Console.Beep();
            }
        }
        
        public static string LeerLinea()
        {
            return ReadLine();
        }
    }
}