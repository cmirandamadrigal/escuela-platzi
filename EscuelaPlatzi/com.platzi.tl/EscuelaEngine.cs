
using Etapa1.com.platzi.bl;

namespace Etapa1.com.platzi.tl
{
    public sealed class EscuelaEngine : Engine
    {
        public EscuelaEngine() : base ()
        {
            
        }
        
        public void RegistrarEscuela(string nombre, int anioCreacion, string pais, string ciudad, TipoEscuela tipoEscuela)
        {
            var tmp = new Escuela(nombre, anioCreacion, tipoEscuela, pais, ciudad);
            
            Logica.RegistrarEscuela(tmp);
        }
        
        public string GetEscuela()
        {
            return Logica.Escuela.ToString();
        }
    }
}