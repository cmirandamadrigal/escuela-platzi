using Etapa1.com.platzi.bl;

namespace Etapa1.com.platzi.tl
{
    public sealed class AsignaturaEngine : Engine
    {

        public void RegistrarAsignatura(string nombre)
        {
            Logica.RegistrarAsignatura(new Asignatura(nombre));
        }

        public Asignatura GetAsignatura(string idAsignatura)
        {
            return Logica.GetAsignatura(idAsignatura);
        }
        
        
        public string[] GetAsignaturas()
        {
            var tmpAlumnos = Logica.Escuela.Asignaturas;

            string[] lista;

            if (tmpAlumnos.Count > 0)
            {
                lista = new string[tmpAlumnos.Count];

                byte i = 0;
                foreach (var tmpEscuela in tmpAlumnos)
                {
                    lista[i] = tmpEscuela.ToString();
                    i++;
                }
            } 
            else
            {
                lista = new []
                {
                    "No se encontraron asignaturas"
                };
            }

            return lista;
        }
    }
}