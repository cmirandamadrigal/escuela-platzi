using Etapa1.com.platzi.bl;

namespace Etapa1.com.platzi.tl
{
    public sealed class AlumnoEngine : Engine
    {

        public void RegistrarAlumno(string nombre)
        {
            var tmpAlumno = new Alumno(nombre);
            
            Logica.RegistrarAlumno(tmpAlumno);
        }
        
        public Alumno GetAlumno(string idAlumno)
        {
            return Logica.GetAlumno(idAlumno);
        }

        public string[] GetAlumnos()
        {
            var tmpAlumnos = Logica.Escuela.Alumnos;

            string[] lista;

            if (tmpAlumnos.Count > 0)
            {
                lista = new string[tmpAlumnos.Count];

                byte i = 0;
                foreach (var tmpEscuela in tmpAlumnos)
                {
                    lista[i] = tmpEscuela.ToString();
                    i++;
                }
            } 
            else
            {
                lista = new []
                {
                    "No se encontraron alumnos"
                };
            }

            return lista;
        }

    }
}