using Etapa1.com.platzi.bl;

namespace Etapa1.com.platzi.tl
{
    public sealed class EvaluacionEngine  : Engine
    {

        public void RegistrarEvaluacion(int nota, Curso curso, Alumno alumno)
        {
            Evaluacion tmpEvaluacion = new Evaluacion()
            {
                Nota = nota,
                Curso = curso,
                Alumno = alumno
            };
            
            Logica.RegistrarEvaluacion(tmpEvaluacion);
        }

        public string[] GetEvaluaciones()
        {
            var tmpEvaluaciones = Logica.Escuela.Evaluaciones;

            string[] lista;

            if (tmpEvaluaciones.Count > 0)
            {
                lista = new string[tmpEvaluaciones.Count];

                byte i = 0;
                foreach (var tmpEscuela in tmpEvaluaciones)
                {
                    lista[i] = tmpEscuela.ToString();
                    i++;
                }
            } 
            else
            {
                lista = new []
                {
                    "No se encontraron evaluaciones"
                };
            }

            return lista;
        }
    }
}