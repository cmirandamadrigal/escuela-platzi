using Etapa1.com.platzi.bl;

namespace Etapa1.com.platzi.tl
{
    public sealed class CursoEngine : Engine
    {
        public void RegistrarCurso(string nombre, TipoJornada tipo, Asignatura asignatura)
        {
            var tmp = new Curso
            {
                Nombre = nombre,
                Jornada = tipo,
                Asignatura = asignatura
            };
            
            Logica.RegistrarCurso(tmp);
        }
        
        public Curso GetCurso(string idCurso)
        {
            return Logica.GetCurso(idCurso);
        }
        
        public string[] GetCursos()
        {
            var tmpCursos = Logica.Escuela.Cursos;

            string[] lista;

            if (tmpCursos.Count > 0)
            {
                lista = new string[tmpCursos.Count];

                byte i = 0;
                foreach (var tmpEscuela in tmpCursos)
                {
                    lista[i] = tmpEscuela.ToString();
                    i++;
                }
            } 
            else
            {
                lista = new []
                {
                    "No se encontraron cursos"
                };
            }

            return lista;
        }
    }
}